#include <stdio.h>
#include <stdlib.h>

#include "NodeList.h"
#include "Graph.h"
int SIZE = 50;


main()
{
    uint i, j;

    // start graph
    Graph *g = Graph_init_random(SIZE, 30);

    // print list of edges for each vertex
    for (i = 0; i < SIZE; i++)
    {
        printf("%d:", i);
        if (NodeList_is_empty(g->edges[i]))
        {
            printf("NONE\n");
        }
        else
        {
            NodeListIterator *itr = Graph_neighbours(g, i);
            while ( itr != 0)
            {
                if (itr->next)
                    printf("%d,", itr->value);
                else
                    printf("%d\n", itr->value);
                itr = itr-> next;
            }
        }
    }

    // deallocate graph
    Graph_delete(g);

}
