TARGET = main randomgraph testPrint mvmatching
LIBS = -lm
CC = gcc
CFLAGS = -g 
# -Wall

.PHONY: default all clean

default: $(TARGET)
all: default

OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))
HEADERS = $(wildcard *.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): OTHER_MAINS = $(filter-out $@, $(TARGET))
$(TARGET): OTHER_MAIN_OBJS = $(patsubst %, %.o, $(OTHER_MAINS))
$(TARGET): THIS_TARGET_OBJS = $(filter-out $(OTHER_MAIN_OBJS), $(OBJECTS))

$(TARGET): $(OBJECTS)
	$(CC) $(THIS_TARGET_OBJS) $(CFLAGS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)