#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "NodeList.h"
#include "Graph.h"
#include "MVInfo.h"

#include "MV.h"

//main()
//{
//    uint i, j;
//
//    // start graph
//    //Graph *g = Graph_init_random(SIZE, modulus);
//
//    // MVInfo *mvi = MVInfo_init_file("g_doubleblosom2.txt");
//    // MVInfo *mvi = MVInfo_init_file("g_paper_figure17.txt");
//    // MVInfo *mvi = MVInfo_init_file("g_aug_2.txt");
//
//    MVInfo *mvi = MVInfo_init_file("g_random1.txt");
//
//
//    // MVInfo_print_graphviz(mvi, "testPrint.dot");
//
//    EdgeList *matching = MV_MaximumCardinalityMatching_(mvi);
//    //EdgeList *matching = MV_MaximumCardinalityMatching(g);
//
//
//    // deallocate
//    EdgeList_delete(matching);
//    Graph *g = mvi->graph;
//    MVInfo_delete(mvi);
//    Graph_delete(g);
//}

int main(int argc, char *argv[])
{
	int size = 0;
	int modulus = 1;

	Graph *g = NULL;
	MVInfo *mvi = NULL;

	uint count;
	uint total_length = 1;

	for (count = 1; count < argc; count++)
		total_length += strlen(argv[count]) + 1;

	char filename[total_length];
	filename[0] = '\0';

	// printf ("This program was called with \"%s\".\n",argv[0]);

	if (argc > 1)
	{
		for (count = 1; count < argc; count++)
		{
			if (str_starts_with(argv[count], "--random"))
			{
				if (sscanf(argv[count], "--random=%d,%d", &size, &modulus) < 2)
				{
					printf("format: --random=SIZE,MODULUS\n");
					return 1;
				}
				if (!size)
				{
					printf("Graph size must be > 0.\n");
					return 1;
				}
				if (g != NULL || mvi != NULL )
				{
					printf(
							"Error processing parameter --random;\nGraph was already set by previous parameter.\n");
					return 1;
				}
				g = Graph_init_random(size, modulus);
			}
			else if (str_starts_with(argv[count], "--file-simple-format"))
			{
				if (sscanf(argv[count], "--file-simple-format=%s", filename)
						< 1)
				{
					printf("format: --file-simple-format=FILENAME\n");
					return 1;
				}
				if (!file_exists(filename))
				{
					printf("No such file, %s.\n", filename);
					return 1;
				}
				if (g != NULL || mvi != NULL )
				{
					printf(
							"Error processing parameter --file-simple-format;\nGraph was already set by previous parameter.\n");
					return 1;
				}
				mvi = MVInfo_init_file(filename);
			}
			else if (str_starts_with(argv[count], "--triangles2"))
			{
				if (sscanf(argv[count], "--triangles2=%d", &size) < 1)
				{
					printf("format: --triangles2=NUM_TRIANGLES\n");
					return 1;
				}
				if (!size)
				{
					printf("Number of triangles must be > 0.\n");
					return 1;
				}
				if (g != NULL || mvi != NULL )
				{
					printf(
							"Error processing parameter --triangles2;\nGraph was already set by previous parameter.\n");
					return 1;
				}
				g = Graph_init_triangles2(size);
			}
			else if (str_starts_with(argv[count], "--triangles"))
			{
				if (sscanf(argv[count], "--triangles=%d", &size) < 1)
				{
					printf("format: --triangles=NUM_TRIANGLES\n");
					return 1;
				}
				if (!size)
				{
					printf("Number of triangles must be > 0.\n");
					return 1;
				}
				if (g != NULL || mvi != NULL )
				{
					printf(
							"Error processing parameter --triangles;\nGraph was already set by previous parameter.\n");
					return 1;
				}
				g = Graph_init_triangles(size);
			}
			else if (str_starts_with(argv[count], "--hardcard"))
			{
				if (sscanf(argv[count], "--hardcard=%d", &size) < 1)
				{
					printf("format: --hardcard=NUM_CLIQUES\n");
					return 1;
				}
				if (!size)
				{
					printf("Number of cliques must be > 0.\n");
					return 1;
				}
				if (g != NULL || mvi != NULL )
				{
					printf(
							"Error processing parameter --hardcard;\nGraph was already set by previous parameter.\n");
					return 1;
				}
				g = Graph_init_hardcard(size);
			}

		}
	}
	else
	{
		printf("mvalgorithm, v0.1.\n\n");

		printf("Usage: mvalgorithm [load-graph-command]\n");

		printf("Commands are:\n");

		printf(
				"  --random=<size>,<modulus>\t\tRandom graph of size <size> where\n  \t\t\t\t\teach edge is picked with probability\n  \t\t\t\t\t<size>/<modulus>.\n");

		printf(
				"  --file-simple-format=<filename>\tload from file, in simple format.\n");
		printf(
				"  --triangles=<num>\t\t\tChain of triangles connected at one\n\t\t\t\t\tvertex. Taken from DIMACS' \"t.f\".");

		printf(
				"  --triangles2=<num>\t\t\tChain of triangles connected at every\n\t\t\t\t\tvertex. Taken from DIMACS' \"tt.f\".");

		printf("\n");
	}

	if (g == NULL && mvi == NULL )
	{
		printf("Must specify a graph to be processed.\n");
		return 1;
	}

	if (g != NULL )
	{
		EdgeList *matching = MV_MaximumCardinalityMatching(g);
		EdgeList_delete(matching);
		Graph_delete(g);
	}
	else if (mvi != NULL )
	{
		EdgeList *matching = MV_MaximumCardinalityMatching_(mvi);
		EdgeList_delete(matching);
		Graph *g = mvi->graph;
		MVInfo_delete(mvi);
		Graph_delete(g);
	}

	return 0;
}
