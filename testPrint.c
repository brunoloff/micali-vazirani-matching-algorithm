#include <stdio.h>
#include <stdlib.h>

#include "NodeList.h"
#include "Graph.h"
#include "MVInfo.h"

int SIZE = 10;


main()
{
    uint i, j;

    // start graph
    Graph *g = Graph_init_random(SIZE,2);

    MVInfo *mvi = MVInfo_init(g);
#ifdef PRINT_MV_PROGRESS
    MVInfo_print_graphviz(mvi, "testPrint.dot");
#endif
    // deallocate
    MVInfo_delete(mvi);
    Graph_delete(g);

}
