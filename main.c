#include <stdio.h>
#include <stdlib.h>

#include "NodeList.h"
#include "Graph.h"
#include "MVInfo.h"

#include "MV.h"

int SIZE = 50;
int modulus = 50 / 2;

main()
{
    uint i, j;

    // start graph
    //Graph *g = Graph_init_random(SIZE, modulus);

    // MVInfo *mvi = MVInfo_init_file("g_doubleblosom2.txt");
    // MVInfo *mvi = MVInfo_init_file("g_paper_figure17.txt");
    // MVInfo *mvi = MVInfo_init_file("g_aug_2.txt");

    MVInfo *mvi = MVInfo_init_file("g_random1.txt");


    // MVInfo_print_graphviz(mvi, "testPrint.dot");

    EdgeList *matching = MV_MaximumCardinalityMatching_(mvi);
    //EdgeList *matching = MV_MaximumCardinalityMatching(g);


    // deallocate
    EdgeList_delete(matching);
    Graph *g = mvi->graph;
    MVInfo_delete(mvi);
    Graph_delete(g);
}
